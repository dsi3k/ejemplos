﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ejemplo1
{
    class Program
    {
        static void Main(string[] args)
        {

            // Registrar designacion de profesor
            Profesor profesor = new Profesor();
            profesor.Nombre = "John";
            profesor.Domicilio = "Balcarce 50";
            profesor.DNI = 9999999;
            profesor.GradoAcademico = "Universitario";

            Materia materia = new Materia();
            materia.Nombre = "Ingenieria superior";
            materia.Carrera = "Ingenieria en sistemas";

            var designacion = new Designacion();
            designacion.Profesor = profesor;
            designacion.FechaIngreso = DateTime.Now;

            var designaciones = new HashSet<Designacion>();
            designaciones.Add(designacion);
            designaciones.Add(designacion);
            designaciones.Add(designacion);
            designaciones.Add(designacion);

            materia.Designaciones = designaciones;

            System.Console.WriteLine(materia.Designaciones.First().Profesor.Nombre);
        }

        class Profesor
        {
           public string Nombre;
           public long DNI;
           public string Domicilio;
           public string GradoAcademico;
        }

        class Materia
        {
            public string Nombre;
            public string Carrera;
            public HashSet<Designacion> Designaciones;
        }

        class Designacion
        {
            public Profesor Profesor;
            public DateTime FechaIngreso;
        }

    }
}



