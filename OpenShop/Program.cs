﻿using System;
using System.Collections.Generic;

namespace OpenShop
{
   
    class GestorVenta
    {
        static Carrito Carrito = new Carrito();

        static void Main(string[] args)
        {
            while (true)
            {
               MostrarProductos();

               var resultado = AgregarAlCarrito();
               if (!resultado)
               {
                   break;
               }
            }

            System.Console.WriteLine("Gracias por comprar");
        }

       static void MostrarProductos()
       {
           System.Console.WriteLine();
           System.Console.WriteLine();
           System.Console.WriteLine();

          foreach (var producto in RegistroProductos.Productos)
          {
              System.Console.WriteLine(producto.Nombre + "  $ " + producto.Precio);
          }
       }

       static bool AgregarAlCarrito()
       {
           System.Console.WriteLine("Seleccione un producto");
           var seleccion = System.Console.ReadLine();

           if (string.IsNullOrEmpty(seleccion))
           {
               return false;
           }

           var producto = RegistroProductos.Productos[int.Parse(seleccion) - 1];

           Carrito.Agregar(producto);
           Carrito.MostrarCarrito();

           return true;
       }
    }

    class RegistroProductos
    {
        public static List<Producto> Productos = new List<Producto>();

        static RegistroProductos()
        {
            Productos.Add(new Producto("Cafetera", 3000));
            Productos.Add(new Producto("Celular", 249999.99m));
            Productos.Add(new Producto("Televisor", 22000));
            Productos.Add(new Producto("Ojotas", 700));
        }
    }

    class Producto
    {
       public string Nombre { get; set; }
       public decimal Precio { get; set; }

       public Producto(string nombre, decimal precio)
       {
           Nombre = nombre;
           Precio = precio;
       }
    }

    class Carrito
    {
       private List<Producto> Productos = new List<Producto>();

       public void Agregar(Producto producto)
       {
          Productos.Add(producto);
       }

       public void MostrarCarrito()
       {
          System.Console.WriteLine("Tienes en tu carrito: ");

           foreach (var productoEnCarrito in Productos)
           {
               System.Console.WriteLine(productoEnCarrito.Nombre);
           }
       }
    }
}
